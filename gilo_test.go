package gilo_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/util"
)

var _ = Describe("gilo", func() {
	defaultBinaryResolver := util.BinaryResolverFunc(func() (string, error) {
		return defaultGiloBinary, nil
	})

	erroringBinaryResolver := util.BinaryResolverFunc(func() (string, error) {
		return "", errWtfError
	})

	simpleEnvironment := gilo.Environment{
		Gopaths: []string{projectDirectory1},
		Path:    util.JoinPaths(defaultGopathBinary),
	}

	nogoenvChangesEnvironment := gilo.Environment{
		Gopaths: []string{defaultGopath},
		Path:    util.JoinPaths(defaultGopathBinary),
	}

	fullEnvironment := gilo.Environment{
		Gopaths:    []string{defaultGopath},
		Gobin:      "Default Gobin",
		Goroot:     "Default Goroot",
		Gorace:     "Default Race",
		Gotooldir:  "Default Tool Dir",
		GoGCCFlags: "Default GCC Flags",
		Path:       util.JoinPaths(defaultGiloBinary, defaultGopathBinary),
	}

	defaultEnvironment := gilo.Environment{
		Gopaths: []string{defaultGopath},
		Path:    util.JoinPaths(defaultGiloBinary, defaultGopathBinary),
	}

	Context("EnvironmentComputer", func() {
		Context("GiloBinary", func() {
			It("should return the error that occurred", func() {
				computer := gilo.EnvironmentComputer{
					DefaultEnv:  defaultEnvironment,
					Transformer: gilo.NewConfigTransformerFromPath(projectDirectory1),
					GiloBinary:  erroringBinaryResolver,
				}

				defaultEnv, computedEnv, err := computer.Compute()
				Expect(err).To(MatchError(errWtfError))
				Expect(defaultEnv).To(Equal(defaultEnvironment))
				Expect(computedEnv).To(Equal(defaultEnvironment))
			})
		})

		Context("Transformer", func() {
			It("should return the default environment if noop transformer is used", func() {
				computer := gilo.EnvironmentComputer{
					DefaultEnv:  defaultEnvironment,
					Transformer: gilo.NoopTransformer,
					GiloBinary:  defaultBinaryResolver,
				}
				// Ignore defaultEnv
				defaultEnv, computedEnv, err := computer.Compute()
				Expect(err).NotTo(HaveOccurred())

				// Expect the default environment to equal the system environment.
				Expect(defaultEnv).To(Equal(defaultEnvironment))
				// Expect the computed environment to equal the system environment.
				Expect(computedEnv).To(Equal(nogoenvChangesEnvironment))
			})

			It("returned computed environment should be equal to simpleEnvironment", func() {
				computer := gilo.EnvironmentComputer{
					DefaultEnv:  defaultEnvironment,
					Transformer: gilo.NewConfigTransformerFromPath(projectDirectory1),
					GiloBinary:  defaultBinaryResolver,
				}

				// Ignore defaultEnv
				_, computedEnv, err := computer.Compute()
				Expect(err).NotTo(HaveOccurred())
				// Expect the computed environment to equal the system environment.
				Expect(computedEnv).To(Equal(simpleEnvironment))
			})

			It("returned default env should be equal to the defaultEnvironment", func() {
				computer := gilo.EnvironmentComputer{
					DefaultEnv:  defaultEnvironment,
					Transformer: gilo.NewConfigTransformerFromPath(projectDirectory1),
					GiloBinary:  defaultBinaryResolver,
				}

				// Ignore computedEnv
				defaultEnv, _, err := computer.Compute()
				Expect(err).NotTo(HaveOccurred())
				// Expect the default environment to equal the system environment.
				Expect(defaultEnv).To(Equal(defaultEnvironment))
			})

			It("should return the default environments values if config is empty", func() {
				// since we're using a noop transformer the only thing that should change
				// from the original environment is the Path.
				expectedEnvironment := gilo.Environment{
					Gobin:      fullEnvironment.Gobin,
					Goroot:     fullEnvironment.Goroot,
					Gorace:     fullEnvironment.Gorace,
					Gotooldir:  fullEnvironment.Gotooldir,
					GoGCCFlags: fullEnvironment.GoGCCFlags,
					Gopaths:    fullEnvironment.Gopaths,
					Path:       util.JoinPaths(defaultGopathBinary),
				}

				computer := gilo.EnvironmentComputer{
					DefaultEnv:  fullEnvironment,
					Transformer: gilo.NoopTransformer,
					GiloBinary:  defaultBinaryResolver,
				}

				defaultEnv, computedEnv, err := computer.Compute()
				Expect(err).NotTo(HaveOccurred())
				// Expect the default environment to equal the system environment.
				Expect(defaultEnv).To(Equal(fullEnvironment))
				Expect(computedEnv).To(Equal(expectedEnvironment))
			})

			It("should override the values provided by the transformer", func() {
				// since we're using a noop transformer the only thing that should change
				// from the original environment is the Path.
				expectedEnvironment := gilo.Environment{
					Gobin:      "Override",
					Goroot:     "Override",
					Gorace:     "Override",
					Gotooldir:  "Override",
					GoGCCFlags: "Override",
					Gopaths:    fullEnvironment.Gopaths,
					Path:       util.JoinPaths(defaultGopathBinary),
				}

				computer := gilo.EnvironmentComputer{
					DefaultEnv: fullEnvironment,
					Transformer: gilo.ConfigTransformerFunc(func(c gilo.Config) (gilo.Config, error) {
						c.Gobin = expectedEnvironment.Gobin
						c.Goroot = expectedEnvironment.Goroot
						c.Gorace = expectedEnvironment.Gorace
						c.Gotooldir = expectedEnvironment.Gotooldir
						c.GoGCCFlags = expectedEnvironment.GoGCCFlags

						return c, nil
					}),
					GiloBinary: defaultBinaryResolver,
				}

				defaultEnv, computedEnv, err := computer.Compute()
				Expect(err).NotTo(HaveOccurred())
				// Expect the default environment to equal the system environment.
				Expect(defaultEnv).To(Equal(fullEnvironment))
				Expect(computedEnv).To(Equal(expectedEnvironment))
			})

			Context("error occurred", func() {
				It("should return the error that occurred", func() {
					computer := gilo.EnvironmentComputer{
						DefaultEnv:  defaultEnvironment,
						Transformer: erroringTransformer{Err: errWtfError},
						GiloBinary:  defaultBinaryResolver,
					}

					defaultEnv, computedEnv, err := computer.Compute()
					Expect(err).To(MatchError(errWtfError))
					Expect(defaultEnv).To(Equal(defaultEnvironment))
					Expect(computedEnv).To(Equal(defaultEnvironment))
				})
			})
		})
	})
})

// ErroringTransformer - Used for tests.
type erroringTransformer struct {
	Err error
}

// Transform - Always errors out and returns an unmodified configuration.
func (t erroringTransformer) Transform(c gilo.Config) (gilo.Config, error) {
	return c, t.Err
}
