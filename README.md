gilo for golang [![Build Status](https://drone.io/bitbucket.org/jatone/gilo/status.png)](https://drone.io/bitbucket.org/jatone/gilo/latest)
=====

Simple Program for setting GOPATH, inspired by rbenv

Intelligently sets up your GOPATH and other golang environment variables.

# Usage
automatically hooks into standard go commands i.e go get, go build, go env, etc.
additional shims can be made to work with other programs (see installation)

# Integrating with gilo
gilo provides a http daemon that can be run and requested for environments for
paths. It returns json payloads describing the golang environment for the specified
path.
```bash
# Default port is 8081
gilod -port=8081 &
curl http://localhost:8081/abs/path/to/directory
```

# Installation ([downloads](https://bitbucket.org/jatone/gilo/downloads))
Currently only support binary builds for linux,osx 386 and amd64.
No support for windows as I don't have a test box, should be fairly easy to add support later.
### latest release
```bash

export RELEASE=0.0.1-linux-amd64
wget https://bitbucket.org/jatone/gilo/downloads/gilo-$RELEASE.tgz
tar xf gilo-$RELEASE.tgz --directory $HOME

# Export bin directory to your path. (or link into the path)
export PATH=$HOME/.gilo/bin:$PATH

# Creating shims
# this will place symlinks to the provided set of programs
# inside of $HOME/.gilo/bin
gilo shim go atom glide
```

### latest (development version)
```bash
GOPATH=$HOME/.gilo go get bitbucket.org/jatone/gilo/commands/...

# Export bin directory to your path. (or link into the path)
export PATH=$HOME/.gilo/bin:$PATH

# Creating shims
# this will place symlinks to the provided set of programs
# inside of $HOME/.gilo/bin
gilo shim go atom glide

```

# How it works
gilo searches the current directory and its parents for a .gopath marker file
and then executes the command and arguments passed to it with the GOPATH and PATH
arguments modified.

# Configuration
```yaml
# See relevant golang documentation for each environment variable.
goroot: "/home/user/.golang/go"
gobin: "/home/user/.local/programs/bin"
gotooldir: "home/user/.golang/tools"
gorace: "log_path=/tmp/race/report strip_path_prefix=/my/go/sources/"
gogccflags: "-fPIC -m64 -pthread -fmessage-length=0"
# gopaths are special in that they are appended to the GOPATH environment variable.
# where as the .gopath directory is prepended. example:
# .gopath resides in /home/soandso/development/project1
# echo $GOPATH is /home/soandso/.golang/lib
# and .gopath contains the array of additional gopaths
# - /home/soandso/development/project2
# the computed GOPATH would be:
# /home/soandos/development/project1:/home/soandso/.golang/lib:/home/soandso/development/project2
gopaths:
  - /home/user/development/project2
```

# Examples
1) create a .gopath file in the directory you want to use as the GOPATH
gilo will search recursively up to root from your current directory.
```bash
$GOPATH
```
```text
bash: /home/jatone/.golang: Is a directory
```
```bash
go env
```
```text
GOARCH="amd64"
GOBIN=""
GOEXE=""
GOHOSTARCH="amd64"
GOHOSTOS="linux"
GOOS="linux"
GOPATH="/shared/development/gilo:/home/jatone/.golang/lib"
GORACE=""
GOROOT="/home/jatone/.golang/go"
GOTOOLDIR="/home/jatone/.golang/go/pkg/tool/linux_amd64"
GO15VENDOREXPERIMENT=""
CC="gcc"
GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0"
CXX="g++"
CGO_ENABLED="1"
```

2) if .gopath is not found it uses the default go env.
```bash
$GOPATH
```
```text
bash: /home/jatone/.golang: Is a directory
```
```bash
go env
```
```text
GOARCH="amd64"
GOBIN=""
GOEXE=""
GOHOSTARCH="amd64"
GOHOSTOS="linux"
GOOS="linux"
GOPATH="/home/jatone/.golang/lib"
GORACE=""
GOROOT="/home/jatone/.golang/go"
GOTOOLDIR="/home/jatone/.golang/go/pkg/tool/linux_amd64"
GO15VENDOREXPERIMENT=""
CC="gcc"
GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0"
CXX="g++"
CGO_ENABLED="1"
```

# Debugging - use gilo env
```bash
gilo env --help
```
```text
usage: gilo env [<path>...]

Display golang environment information for the supplied paths

Flags:
  --help     Show context-sensitive help (also try --help-long and --help-man).
  --version  Show application version.

Args:
  [<path>]  path to display golang environment, defaults to current directory
```
```bash
gilo env
```
```text
2016/02/13 11:16:35 Golang Environment for: /shared/development/gilo
Default Go Environment:
GOROOT: /home/jatone/.golang/go
GOBIN:
GORACE:
GOTOOLDIR:
GOGCCFLAGS:
GOPATH: /home/jatone/.golang/lib
PATH: /home/jatone/.local/programs/bin:/home/jatone/.rbenv/shims:/home/jatone/.rbenv/bin:/home/jatone/.gilo/bin:/home/jatone/.golang/go/bin:/home/jatone/.golang/lib/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl
Computed Go Environment:
GOROOT: /home/jatone/.golang/go
GOBIN:
GORACE:
GOTOOLDIR:
GOGCCFLAGS:
GOPATH: /shared/development/gilo:/home/jatone/.golang/lib
PATH: /home/jatone/.local/programs/bin:/home/jatone/.rbenv/shims:/home/jatone/.rbenv/bin:/home/jatone/.golang/go/bin:/home/jatone/.golang/lib/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl
```

# Debugging multiple paths
```bash
gilo env path1 path2 path3
```
```text
program output
same as gilo env but for each path.
```

# invoke arbitrary command with gilo
```bash
gilo-shim mycommand commandargs...
```

# Upcoming features:
  - be able to specify the file to search for along the directory path.
  this will enable setting different configurations for activities like cross compiling.
