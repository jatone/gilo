package gilo

import (
	"os"
	"path/filepath"

	"bitbucket.org/jatone/gilo/util"
)

// Config - Configuration for gilo.
// allows specifying various golang environment variables.
type Config struct {
	Goroot     string
	Gobin      string
	Gorace     string
	Gotooldir  string
	GoGCCFlags string
	Gopaths    []string
}

// ConfigFromEnvironment - creates a config from the environment.
func ConfigFromEnvironment(env Environment) Config {
	return Config{
		Goroot:     env.Goroot,
		Gobin:      env.Gobin,
		Gorace:     env.Gorace,
		Gotooldir:  env.Gotooldir,
		GoGCCFlags: env.GoGCCFlags,
		Gopaths:    env.Gopaths,
	}
}

// ConfigTransformer - Responsible for taking a base configuration
// and transforming it into another configuration.
// A ConfigTransformer should not return any updated values
// in the returned configuration if an error occurs. i.e.
// when an error occurs the input config should be equal to the output config.
type ConfigTransformer interface {
	Transform(Config) (Config, error)
}

// ConfigTransformerFunc - Allows pure functions to act as ConfigTransformers.
type ConfigTransformerFunc func(Config) (Config, error)

// Transform - Invokes the pure function on the specified configuration.
func (t ConfigTransformerFunc) Transform(config Config) (Config, error) {
	return t(config)
}

// ConfigTransformerFactory - Builds a ConfigTransformer
type ConfigTransformerFactory struct {
	util.FileResolver
}

// Build - Takes a path and returns either a ConfigTransformer or an error.
func (t ConfigTransformerFactory) Build(name string) (ConfigTransformer, error) {
	giloconfig, err := t.FileResolver.Find(name)
	switch err {
	// Happy case configuration file found.
	case nil:
		Debug.Println("configuration found")
		return MustNewTransformers(giloconfig), nil
	// do nothing, just use NoopTransformer to provide the default
	// golang environment.
	case util.ErrFileNotFound:
		Debug.Println("noop transformer")
		return NoopTransformer, nil
	// some unexpected error was encountered, short circuit.
	default:
		return nil, err
	}
}

// CompositeConfigTransformer - Takes a set of transformers and applies each one to the
// the input configuration. If any of the transformers fail, then the composite transformer
// fails immediately and returns the error.
func CompositeConfigTransformer(transformers ...ConfigTransformer) ConfigTransformer {
	return ConfigTransformerFunc(func(in Config) (c Config, err error) {
		c = in
		Debug.Println("transformers:", len(transformers))
		for idx, transformer := range transformers {
			if c, err = transformer.Transform(c); err != nil {
				return c, err
			}
			Debug.Printf("transformation %d - %#v\n", idx, c)
		}
		return c, nil
	})
}

// NewConfigTransformerFromPath - Creates a new configuration with default values from
// the specified path. Completely ignores the provided configuration.
func NewConfigTransformerFromPath(path string) ConfigTransformer {
	return ConfigTransformerFunc(func(c Config) (Config, error) {
		c.Gopaths = []string{path}
		return c, nil
	})
}

// NoopTransformer - Does not make any changes to the configuration.
// Used for Tests.
var NoopTransformer = ConfigTransformerFunc(func(c Config) (Config, error) {
	return c, nil
})

// NewTransformers - Generates the set of Configuration Transformers needed based
// on the provided filepath to a configuration file.
func NewTransformers(giloconfigpath string) (ConfigTransformer, error) {
	configFile, err := os.Open(giloconfigpath)
	if err != nil {
		return nil, err
	}

	// Take the directory of the giloconfigpath so we properly generate the
	// base configuration.
	baseTransformer := NewConfigTransformerFromPath(filepath.Dir(giloconfigpath))
	yamlTransformer := YAMLConfig{Reader: configFile}
	return CompositeConfigTransformer(baseTransformer, yamlTransformer), nil
}

// MustNewTransformers - Panics if the delegated invocation of NewTransformers
// errors.
func MustNewTransformers(path string) ConfigTransformer {
	var transformer ConfigTransformer
	var err error

	if transformer, err = NewTransformers(path); err == nil {
		return transformer
	}

	panic(err)
}
