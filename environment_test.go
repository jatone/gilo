package gilo_test

import (
	"bytes"
	"fmt"
	"os"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Environment", func() {
	simpleEnvironment := gilo.Environment{
		Gopaths: []string{defaultGopath},
		Path:    util.JoinPaths(projectDirectory1, "/usr/bin"),
	}

	// remember and make assertions about the default environment for sanity sake.
	originalEnvironment := gilo.DefaultEnvironment()
	BeforeEach(func() {
		Expect(os.Getenv("GOROOT")).To(Equal(originalEnvironment.Goroot))
		Expect(os.Getenv("GOBIN")).To(Equal(originalEnvironment.Gobin))
		Expect(os.Getenv("GORACE")).To(Equal(originalEnvironment.Gorace))
		Expect(os.Getenv("GOTOOLDIR")).To(Equal(originalEnvironment.Gotooldir))
		Expect(os.Getenv("GOGCCFLAGS")).To(Equal(originalEnvironment.GoGCCFlags))
	})

	AfterEach(func() {
		// reset the environment each time.
		originalEnvironment.MungeEnvironment()
	})

	Context("DefaultEnvironment", func() {
		It("use options provided", func() {
			defaultEnv := gilo.DefaultEnvironment()
			createdEnv := gilo.DefaultEnvironment(gilo.EnvironmentOptionGopath("/home/user/.golang"))
			Expect(createdEnv.Goroot).To(Equal(defaultEnv.Goroot))
			Expect(createdEnv.Gobin).To(Equal(defaultEnv.Gobin))
			Expect(createdEnv.Gorace).To(Equal(defaultEnv.Gorace))
			Expect(createdEnv.Gotooldir).To(Equal(defaultEnv.Gotooldir))
			Expect(createdEnv.GoGCCFlags).To(Equal(defaultEnv.GoGCCFlags))
			Expect(createdEnv.Gopaths).To(Equal([]string{"/home/user/.golang"}))
		})
	})

	Context("PrintEnvironment", func() {
		It("should print out the environment", func() {
			expected := fmt.Sprintf(
				"GOROOT: %s\nGOBIN: %s\nGORACE: %s\nGOTOOLDIR: %s\nGOGCCFLAGS: %s\nGOPATH: %s\nPATH: %s\n",
				originalEnvironment.Goroot,
				originalEnvironment.Gobin,
				originalEnvironment.Gorace,
				originalEnvironment.Gotooldir,
				originalEnvironment.GoGCCFlags,
				originalEnvironment.Gopath(),
				originalEnvironment.Path,
			)
			w := bytes.NewBuffer([]byte{})
			Expect(gilo.PrintEnvironment(w, originalEnvironment)).ToNot(HaveOccurred())
			Expect(w.String()).To(Equal(expected))
		})

		It("should return an error if any", func() {
			expectedErr := fmt.Errorf("BOOM")
			w := errWriter{err: expectedErr}
			Expect(gilo.PrintEnvironment(w, originalEnvironment)).To(MatchError(expectedErr))
		})
	})

	Context("StripFromPath", func() {
		It("should strip the specified subpath from the environment path", func() {
			var env gilo.Environment

			// Strip from start
			env = gilo.Environment{
				Path: "/a/b/c:/d/e/f:/g/h/i",
			}
			env = env.StripFromPath("/a/b/c")
			Expect(env.Path).To(Equal("/d/e/f:/g/h/i"))

			// Strip from middle
			env = gilo.Environment{
				Path: "/a/b/c:/d/e/f:/g/h/i",
			}
			env = env.StripFromPath("/d/e/f")
			Expect(env.Path).To(Equal("/a/b/c:/g/h/i"))

			// Strip from end
			env = gilo.Environment{
				Path: "/a/b/c:/d/e/f:/g/h/i",
			}
			env = env.StripFromPath("/g/h/i")
			Expect(env.Path).To(Equal("/a/b/c:/d/e/f"))

			// Strip repeats
			env = gilo.Environment{
				Path: "/a/b/c:/d/e/f:/d/e/f:/g/h/i",
			}
			env = env.StripFromPath("/d/e/f")
			Expect(env.Path).To(Equal("/a/b/c:/g/h/i"))
		})
	})

	Context("UpdateFromConfig", func() {
		It("should override the gopaths of the environment with the gopaths of the config", func() {
			config := gilo.Config{
				Gopaths: []string{projectDirectory1},
			}
			env := simpleEnvironment.UpdateFromConfig(config)
			// ensure we don't clobbered the values from the environment.
			Expect(env.Gopaths).NotTo(Equal(simpleEnvironment.Gopaths))
			Expect(env.Gopaths).To(Equal([]string{projectDirectory1}))
		})
	})

	Context("ReplaceWithGobinInPath", func() {
		It("should replace first occurrance with environments gobin", func() {
			env := gilo.Environment{
				Gopaths: []string{projectDirectory1},
				Gobin:   projectDirectory1Binary,
				Path:    util.JoinPaths(defaultGiloBinary, defaultGiloBinary, defaultGopathBinary, "/usr/bin"),
			}

			env = env.ReplaceWithGobinInPath(defaultGiloBinary)
			Expect(env.Path).To(Equal(util.JoinPaths(projectDirectory1Binary, defaultGiloBinary, defaultGopathBinary, "/usr/bin")))
		})
	})

	Context("StripFromPath", func() {
		It("should strip all occurrances of the subpath from the environment's path", func() {
			env := gilo.Environment{
				Path: util.JoinPaths(defaultGiloBinary, defaultGiloBinary, defaultGopathBinary, "/usr/bin"),
			}

			env = env.StripFromPath(defaultGiloBinary)
			Expect(env.Path).To(Equal(util.JoinPaths(defaultGopathBinary, "/usr/bin")))
		})
	})

	Context("MungeEnvironment", func() {
		It("should properly update all appropriate environment variables", func() {
			env := gilo.Environment{
				Goroot:     "/home/user/.golang/go",
				Gobin:      "/home/user/.local/programs/bin",
				Gotooldir:  "gome/user/.golang/tools",
				Gorace:     "log_path=/tmp/race/report strip_path_prefix=/my/go/sources/",
				GoGCCFlags: "-fPIC -m64 -pthread -fmessage-length=0",
				Gopaths:    []string{"/home/user/development/goproject1"},
			}

			Expect(os.Getenv("GOROOT")).ToNot(Equal(env.Goroot))
			Expect(os.Getenv("GOBIN")).ToNot(Equal(env.Gobin))
			Expect(os.Getenv("GORACE")).ToNot(Equal(env.Gorace))
			Expect(os.Getenv("GOTOOLDIR")).ToNot(Equal(env.Gotooldir))
			Expect(os.Getenv("GOGCCFLAGS")).ToNot(Equal(env.GoGCCFlags))
			env.MungeEnvironment()
			Expect(os.Getenv("GOROOT")).To(Equal(env.Goroot))
			Expect(os.Getenv("GOBIN")).To(Equal(env.Gobin))
			Expect(os.Getenv("GORACE")).To(Equal(env.Gorace))
			Expect(os.Getenv("GOTOOLDIR")).To(Equal(env.Gotooldir))
			Expect(os.Getenv("GOGCCFLAGS")).To(Equal(env.GoGCCFlags))
		})
	})
})

type errWriter struct {
	err error
}

func (t errWriter) Write(_ []byte) (int, error) {
	return 0, t.err
}
