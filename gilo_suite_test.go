package gilo_test

import (
	"fmt"
	"path/filepath"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

// Useful test variables.
var (
	defaultGiloDirectory    = filepath.Join("/", "home", "user", ".gilo")
	defaultGiloBinary       = filepath.Join("/", defaultGiloDirectory, "bin")
	defaultGopath           = filepath.Join("/", "home", "user", ".golang")
	defaultGopathBinary     = filepath.Join(defaultGopath, "bin")
	projectDirectory1       = filepath.Join("/", "home", "user", "development", "goproject1")
	projectDirectory1Binary = filepath.Join(projectDirectory1, "bin")
	errWtfError             = fmt.Errorf("some really unexpected error")
)

func TestGilo(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Gilo Suite")
}
