package gilo_test

import (
	"bytes"

	"bitbucket.org/jatone/gilo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var emptyYAMLConfiguration = ``
var fullYAMLConfiguration = `
goroot: "/home/user/.golang/go"
gobin: "/home/user/.local/programs/bin"
gotooldir: "gome/user/.golang/tools"
gorace: "log_path=/tmp/race/report strip_path_prefix=/my/go/sources/"
gogccflags: "-fPIC -m64 -pthread -fmessage-length=0"
gopaths:
  - "/home/user/development/goproject1"
`

var gobinOnlyYAMLConfiguration = `
gobin: "/home/user/.local/programs/bin"
`
var _ = Describe("YamlConfig", func() {
	emptyConfig := gilo.Config{}
	gopathsConfig := gilo.Config{
		Gopaths: []string{defaultGopath},
	}
	fullConfig := gilo.Config{
		Gopaths: []string{projectDirectory1},
		Gobin:   projectDirectory1Binary,
		Gorace:  "custom",
	}

	Context("transform", func() {
		It("should transform the provided config based on the values in the yaml", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(fullYAMLConfiguration))}
			transformed, err := transformer.Transform(emptyConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).ToNot(Equal(emptyConfig))
			Expect(transformed.Goroot).To(Equal("/home/user/.golang/go"))
			Expect(transformed.Gobin).To(Equal("/home/user/.local/programs/bin"))
			Expect(transformed.Gorace).To(Equal("log_path=/tmp/race/report strip_path_prefix=/my/go/sources/"))
			Expect(transformed.Gotooldir).To(Equal("gome/user/.golang/tools"))
			Expect(transformed.GoGCCFlags).To(Equal("-fPIC -m64 -pthread -fmessage-length=0"))
			Expect(transformed.Gopaths).To(Equal([]string{projectDirectory1}))
		})

		It("should merge gopaths from yaml and base configuration", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(fullYAMLConfiguration))}
			transformed, err := transformer.Transform(gopathsConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).ToNot(Equal(gopathsConfig))
			Expect(transformed.Gobin).To(Equal("/home/user/.local/programs/bin"))
			Expect(transformed.Gopaths).To(Equal([]string{defaultGopath, projectDirectory1}))
		})

		It("should retain the gopaths from the base configuration if none are provided in the yaml", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(gobinOnlyYAMLConfiguration))}
			transformed, err := transformer.Transform(gopathsConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).ToNot(Equal(gopathsConfig))
			Expect(transformed.Gopaths).To(Equal(gopathsConfig.Gopaths))
		})

		It("should override base config gobin if yaml specifies a gobin", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(gobinOnlyYAMLConfiguration))}
			transformed, err := transformer.Transform(fullConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).ToNot(Equal(fullConfig))
			Expect(transformed.Gobin).To(Equal("/home/user/.local/programs/bin"))
		})

		It("should not override base config gobin if yaml lacks a gobin", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(emptyYAMLConfiguration))}
			transformed, err := transformer.Transform(fullConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).To(Equal(fullConfig))
			Expect(transformed.Gobin).ToNot(BeEmpty())
		})

		It("should return the configuration unchanged when yaml is empty", func() {
			transformer := gilo.YAMLConfig{Reader: bytes.NewBuffer([]byte(emptyYAMLConfiguration))}
			transformed, err := transformer.Transform(fullConfig)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformed).To(Equal(fullConfig))
			Expect(transformed.Gobin).To(Equal(projectDirectory1Binary))
			Expect(transformed.Gopaths).To(Equal([]string{projectDirectory1}))
		})
	})
})
