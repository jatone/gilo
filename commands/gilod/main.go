package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"gopkg.in/alecthomas/kingpin.v2"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/x/stringsx"
)

// Usage for the gilod - http://localhost:8095/absolute/path/to/directory
// For example if you wanted the gilo golang environment for your project at:
// /home/user/development/project1 then you would request:
// http://localhost:8095/home/user/development/project1

var (
	app     = kingpin.New("gilod", "golang environment manager daemon")
	version = app.Version(stringsx.DefaultIfBlank(gilo.Version, gilo.DefaultVersion))
)

func main() {
	port := app.Arg("port", "port daemon listens on").Default("8081").Int()
	kingpin.MustParse(app.Parse(os.Args[1:]))

	log.Fatal(http.ListenAndServe(fmt.Sprintf("localhost:%d", *port), http.HandlerFunc(generateGolangEnvironment)))
}

func generateGolangEnvironment(w http.ResponseWriter, r *http.Request) {
	_, computedEnv, err := gilo.GolangEnvForPath(r.URL.Path)
	switch err {
	// Meh case fix, fix your environment.
	case gilo.ErrDuplicateBinaryDirectory:
		log.Println("WARN", err)
		fallthrough
	// Happy case
	case nil:
		golangEnv := struct {
			Gobin, Gopath, Path string
		}{
			Gobin:  computedEnv.Gobin,
			Gopath: computedEnv.Gopath(),
			Path:   computedEnv.Path,
		}

		if err = json.NewEncoder(w).Encode(golangEnv); err != nil {
			log.Println("ERROR", err)
		}
	// WTF case.
	default:
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
