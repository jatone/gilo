package main

import (
	"log"
	"os"
	"os/exec"
	"io/ioutil"
	"strings"
	"syscall"
	"path/filepath"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/util"
)

func main() {
	var err error
	if os.Getenv("GILO_DEBUG") != "" {
			log.Println(os.Getenv("GILO_DEBUG"))
			f, err := os.OpenFile("gilodebug.log", os.O_RDWR | os.O_CREATE | os.O_APPEND | os.O_SYNC, 0666)
			if err != nil {
		    	log.Fatalf("error opening file: %v", err)
			}
			defer f.Close()
			log.SetOutput(f)
	} else {
			log.SetOutput(ioutil.Discard)
	}

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("commandline args", os.Args)
	command, args := os.Args[0], os.Args[:]
	// All shimmed commands bypass this section.
	if strings.HasSuffix(command, gilo.ShimBinaryName) {
		log.Println("had gilo.ShimBinaryName:", gilo.ShimBinaryName, "Args:", len(args))
		switch len(args) {
		// pretty sure 0 case can't happen. better safe than sorry.
		case 0:
			fallthrough
		case 1:
			log.Fatal("gilo-shim is not suppose to be run by itself. use gilo.")
		// gilo-shim go -> len 2 -> go go
		// gilo-shim go version -> len 3 -> go go version
		default:
			log.Println("args[1]", args[1], "args[1:]", args[1:])
			command, args = args[1], args[1:]
		}
	}

	_, computedEnv, err := gilo.GolangEnvForPath(util.MustWorkingDirectory())
	switch err {
	// Happy case
	case nil:
	// Meh case, fix your environment.
	case gilo.ErrDuplicateBinaryDirectory:
		log.Println("WARN", err)
	// WTF case.
	default:
		log.Fatal(err)
	}

	// Setup the environment for the command.
	computedEnv.MungeEnvironment()

	actualCommand, err := exec.LookPath(command)
	if err != nil {
		log.Fatal(err)
	}

	// if the actual command is the same as the new command path
	// means they were looking for the path to the executable we are shimming.
	if actualCommand == command {
		actualCommand, err = exec.LookPath(filepath.Base(actualCommand))
		if err != nil {
			log.Fatal(err)
		}
		args[0] = actualCommand
	}

	log.Println("execing", actualCommand, args, os.Getenv("GOPATH"), os.Getenv("PATH"))
	if err = syscall.Exec(actualCommand, args, os.Environ()); err != nil {
		log.Fatalln(err)
	}
}
