package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/x/stringsx"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app     = kingpin.New("gilo", "golang environment manager")
	version = app.Version(stringsx.DefaultIfBlank(gilo.Version, gilo.DefaultVersion))
)

func main() {
	(&env{}).configure(app)
	(&shim{}).configure(app)

	kingpin.MustParse(app.Parse(os.Args[1:]))
}

func printDebug(path string, defaultEnv, computedEnv gilo.Environment, err error) {
	log.Println("Golang Environment for:", path)
	if err != nil {
		fmt.Println("An error occurred generating environment:", err)
	} else {
		fmt.Println("Default Go Environment:")
		gilo.PrintEnvironment(os.Stdout, defaultEnv)
		fmt.Println("Computed Go Environment:")
		gilo.PrintEnvironment(os.Stdout, computedEnv)
	}
}
