package main

import (
	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/util"

	"gopkg.in/alecthomas/kingpin.v2"
)

type env struct {
	paths []string
}

func (t *env) execute(_ *kingpin.ParseContext) error {
	for _, path := range t.paths {
		defaultEnv, computedEnv, err := gilo.GolangEnvForPath(path)
		printDebug(path, defaultEnv, computedEnv, err)
	}
	return nil
}

func (t *env) configure(app *kingpin.Application) *kingpin.CmdClause {
	env := app.Command("env", "Display golang environment information for the supplied paths")
	env.Arg("path", "path to display golang environment, defaults to current directory").
		Default(util.MustWorkingDirectory()).StringsVar(&t.paths)
	env.Action(t.execute)
	return env
}
