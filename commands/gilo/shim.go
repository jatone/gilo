package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"bitbucket.org/jatone/gilo"

	"gopkg.in/alecthomas/kingpin.v2"
)

type shim struct {
	binary    string
	directory string
	commands  []string
}

func (t *shim) execute(_ *kingpin.ParseContext) error {
	var (
		err    error
		binary string
	)

	if binary, err = exec.LookPath(t.binary); err != nil {
		return err
	}

	for _, cmd := range t.commands {
		newname := filepath.Join(t.directory, cmd)
		fmt.Println("linking", binary, "to", newname)
		if err := os.Symlink(binary, newname); err != nil {
			return err
		}
	}

	return nil
}

func (t *shim) configure(app *kingpin.Application) *kingpin.CmdClause {
	var (
		err    error
		binary string
	)

	if binary, err = exec.LookPath(gilo.ShimBinaryName); err != nil {
		log.Fatalln("failed to find the gilo shim binary, you're environment is not properly setup.", err)
	}

	cmd := app.Command("shim", "creates a shim for the given program")
	cmd.Flag("shim-command", "name of program acting as the shim").
		Default(binary).StringVar(&t.binary)
	cmd.Flag("shim-directory", "location to place the created shim").
		Default(filepath.Dir(binary)).StringVar(&t.directory)
	cmd.Arg("commands", "name of the commands to create shims for").Required().
		StringsVar(&t.commands)

	cmd.Action(t.execute)

	return cmd
}
