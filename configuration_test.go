package gilo_test

import (
	"os"
	"path/filepath"

	"bitbucket.org/jatone/gilo"
	"bitbucket.org/jatone/gilo/util"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Configuration", func() {
	Context("NewTransformers", func() {
		It("should return an error if file does not exist", func() {
			transformer, err := gilo.NewTransformers("I_Do_Not_Exist")
			Expect(err).To(MatchError("open I_Do_Not_Exist: no such file or directory"))
			Expect(transformer).To(BeNil())
		})

		It("should return a config transformer when file exists", func() {
			filepath := filepath.Join(os.TempDir(), "I_Exist")
			defer os.Remove(filepath)

			_, err := os.Create(filepath)
			Expect(err).ToNot(HaveOccurred())

			transformer, err := gilo.NewTransformers(filepath)
			Expect(err).ToNot(HaveOccurred())
			Expect(transformer).ToNot(BeNil())
		})
	})

	Context("ConfigTransformerFactory", func() {
		It("should return an error if some unexpected error occurrs", func() {
			factory := gilo.ConfigTransformerFactory{
				FileResolver: util.ErroringResolver{Err: errWtfError},
			}

			transformer, err := factory.Build("blah")
			Expect(err).To(HaveOccurred())
			Expect(transformer).To(BeNil())
		})

		It("should return a noop transformer if the path doesn't exist", func() {
			factory := gilo.ConfigTransformerFactory{
				FileResolver: util.RecursiveResolver{Directory: os.TempDir()},
			}

			transformer, err := factory.Build("I_Do_Not_Exist")
			Expect(err).ToNot(HaveOccurred())
			Expect(transformer.Transform(gilo.Config{})).To(Equal(gilo.Config{}))
		})

		It("should return a transformer if the file exists", func() {
			path := filepath.Join(os.TempDir(), "I_Exist")
			expectedConfig := gilo.Config{
				Gopaths: []string{os.TempDir()},
			}

			_, err := os.Create(path)
			Expect(err).ToNot(HaveOccurred())
			defer os.Remove(path)

			factory := gilo.ConfigTransformerFactory{
				FileResolver: util.RecursiveResolver{Directory: os.TempDir()},
			}

			transformer, err := factory.Build("I_Exist")
			Expect(err).ToNot(HaveOccurred())
			Expect(transformer.Transform(gilo.Config{})).To(Equal(expectedConfig))
		})
	})
})
