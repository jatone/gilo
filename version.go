package gilo

const (
	// DefaultVersion used if version isn't set.
	DefaultVersion = "development"
)

// Version specified at build time using: `git describe --tags --long`
var Version string
