COMMIT ?= master
PACKAGE ?= bitbucket.org/jatone/gilo/commands
VERSION = $(shell git describe --tags --long $(COMMIT))
RELEASE = $(shell git describe --tags --long $(COMMIT) | sed 's/\(.*\)-.*/\1/')
LDFLAGS ?= "-X bitbucket.org/jatone/gilo.Version=$(VERSION)"

setup:
	git checkout $(COMMIT)

generate:
	go generate $(go list ./... | grep -v /vendor/)

build: generate
	go install -ldflags=$(LDFLAGS) $(PACKAGE)/...

package: package-clean setup generate
	echo "packaging version: $(VERSION)"
	$(call build_binary,$(PACKAGE),linux,amd64,$(CURDIR)/.release)
	$(call build_binary,$(PACKAGE),linux,386,$(CURDIR)/.release)
	$(call build_binary,$(PACKAGE),darwin,amd64,$(CURDIR)/.release)

package-clean:
	rm -rf .release/bin

test:
	ginkgo -p -r -cover -keepGoing

release: release-check release-clean release-tag package
	echo "released version: $(RELEASE) completed successfully"

release-tag:
ifneq ($(origin tag), undefined)
	git tag $(tag)
endif

release-check:
ifeq ($(origin ALLOW_DIRTY), undefined)
	git diff --exit-code --quiet || { echo repository has uncommitted files. set ALLOW_DIRTY to ignore this check; exit 1; }
endif

release-clean:
	rm -rf .release

define build_binary
	mkdir -p $(4)
	GOOS=$(2) GOARCH=$(3) go build -ldflags=$(LDFLAGS) -o $(4)/bin/$(2)_$(3)/gilo $(1)/gilo
	GOOS=$(2) GOARCH=$(3) go build -ldflags=$(LDFLAGS) -o $(4)/bin/$(2)_$(3)/gilo-shim $(1)/gilo-shim
	GOOS=$(2) GOARCH=$(3) go build -ldflags=$(LDFLAGS) -o $(4)/bin/$(2)_$(3)/gilod $(1)/gilod
	tar --directory $(4)/bin --transform="s|$(2)_$(3)|.gilo/bin|" -czf $(4)/gilo-$(RELEASE)-$(2)-$(3).tgz $(2)_$(3)
endef
