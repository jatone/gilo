package gilo

import (
	"errors"
	"io/ioutil"
	"log"
	"strings"

	"bitbucket.org/jatone/gilo/util"
)

// ShimBinaryName name of the shim binary name
const ShimBinaryName = "gilo-shim"

// ConfigFileName - file name containing the gilo configuration.
const ConfigFileName = ".gopath"

// ErrDuplicateBinaryDirectory - Usually indicates an environment problem and gilo may behave unexpectedly.
// For example infinite loops. Gilo Attempts to bypass this behaviour by stripping all occurrances
// of the gilo bin directory from the path, but if other binaries are installed at that location then
// they may not be properly resolved within the executed environment.
var ErrDuplicateBinaryDirectory = errors.New("found multiple instances of the gilo bin directory within the path")

// Debug logger, defaults to discard
var Debug = log.New(ioutil.Discard, "DEBUG ", log.LstdFlags|log.Lshortfile)

// EnvironmentComputer - Computes the default and configured golang environments.
type EnvironmentComputer struct {
	// DefaultEnv - the default golang environment, used as a base for
	// the computed golang environment.
	DefaultEnv Environment
	// Transformer - Applies transformations to the configuration.
	Transformer ConfigTransformer
	// GiloBinary - Returns the directory path where the gilo binary
	// is located, used to strip the binary from the environments path.
	// allowing gilo to act as a shim for various commands.
	GiloBinary util.BinaryResolver
}

// Compute - Computes the default golang environment and the updated golang
// environment based on the provided resolver.
func (t EnvironmentComputer) Compute() (Environment, Environment, error) {
	var (
		err     error
		gilobin string
	)
	config := ConfigFromEnvironment(t.DefaultEnv)

	Debug.Printf("original configuration %#v\n", config)

	// Transform the configuration based on the provided transformers
	if config, err = t.Transformer.Transform(config); err != nil {
		return t.DefaultEnv, t.DefaultEnv, err
	}

	Debug.Printf("transformed configuration %#v\n", config)

	// Clear the default gopath from the environment.
	// Then update the environment based on the generated configuration.
	computed := NewEnvironment(
		t.DefaultEnv,
		EnvironmentOptionGopath(),
	).UpdateFromConfig(config)

	if gilobin, err = t.GiloBinary.Path(); err != nil {
		// intentionally return the original environment.
		return t.DefaultEnv, t.DefaultEnv, err
	}

	// check path for oddities. (such as containing gilo binary directory more than once.)
	if strings.Count(computed.Path, gilobin) > 1 {
		err = ErrDuplicateBinaryDirectory
	}

	// strip the gilo bin directory with the gobin directory in the path.
	computed = computed.StripFromPath(gilobin)

	return t.DefaultEnv, computed, err
}

// GolangEnvForPath - Takes a path and returns the default and computed environments.
func GolangEnvForPath(path string) (Environment, Environment, error) {
	var (
		err         error
		transformer ConfigTransformer
	)

	defaultEnv := DefaultEnvironment()

	transformerFactory := ConfigTransformerFactory{
		FileResolver: util.RecursiveResolver{Directory: path},
	}

	if transformer, err = transformerFactory.Build(ConfigFileName); err != nil {
		return defaultEnv, defaultEnv, err
	}

	// EnvironmentComputer initialization
	computer := EnvironmentComputer{
		DefaultEnv:  defaultEnv,
		Transformer: transformer,
		GiloBinary:  util.GiloBinary,
	}

	// Computing
	return computer.Compute()
}
