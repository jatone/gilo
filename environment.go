package gilo

import (
	"fmt"
	"io"
	"os"

	"bitbucket.org/jatone/gilo/util"
)

// EnvironmentOption specifies an option for the environment
type EnvironmentOption func(*Environment)

// EnvironmentOptionGopath sets the gopaths for the environment
func EnvironmentOptionGopath(paths ...string) EnvironmentOption {
	return func(env *Environment) {
		if len(paths) > 0 {
			env.Gopaths = paths
		} else {
			env.Gopaths = []string{}
		}
	}
}

// NewEnvironment creates a new environment using the template and the options.
func NewEnvironment(template Environment, options ...EnvironmentOption) Environment {
	for _, opt := range options {
		opt(&template)
	}

	return template
}

// DefaultEnvironment -  Loads golang default environment from environment variables.
func DefaultEnvironment(options ...EnvironmentOption) Environment {
	env := Environment{
		Goroot:     os.Getenv("GOROOT"),
		Gobin:      os.Getenv("GOBIN"),
		Gorace:     os.Getenv("GORACE"),
		Gotooldir:  os.Getenv("GOTOOLDIR"),
		GoGCCFlags: os.Getenv("GOGCCFLAGS"),
		Gopaths:    []string{os.Getenv("GOPATH")},
		Path:       os.Getenv("PATH"),
	}

	return NewEnvironment(env, options...)
}

// Environment - Specifies a golang environment.
type Environment struct {
	Goroot     string
	Gobin      string
	Gorace     string
	Gotooldir  string
	GoGCCFlags string
	Gopaths    []string
	Path       string // Path environment variable, we update it based on the golang configuration.
}

// StripFromPath - Updates the environment's Path by stripping the specified subpath
// from the path.
func (t Environment) StripFromPath(subpath string) Environment {
	t.Path = util.UnixPathMunger(t.Path).Strip(subpath)
	return t
}

// ReplaceWithGobinInPath - Replaces the first occurrance of the given string with the
// Environment's Gobin value.
func (t Environment) ReplaceWithGobinInPath(pattern string) Environment {
	t.Path = util.UnixPathMunger(t.Path).Replace(pattern, t.Gobin, 1)
	return t
}

// UpdateFromConfig - Updates the environment using the specified configuration.
func (t Environment) UpdateFromConfig(config Config) Environment {
	return Environment{
		Goroot:     config.Goroot,
		Gobin:      config.Gobin,
		Gorace:     config.Gorace,
		Gotooldir:  config.Gotooldir,
		GoGCCFlags: config.GoGCCFlags,
		Gopaths:    config.Gopaths,
		Path:       t.Path,
	}
}

// Gopath - Returns the list seperated (:) gopath.
func (t Environment) Gopath() string {
	return util.JoinPaths(t.Gopaths...)
}

// MungeEnvironment - munges the environment variables with updated values.
func (t Environment) MungeEnvironment() {
	os.Setenv("GOROOT", t.Goroot)
	os.Setenv("GOBIN", t.Gobin)
	os.Setenv("GORACE", t.Gorace)
	os.Setenv("GOTOOLDIR", t.Gotooldir)
	os.Setenv("GOGCCFLAGS", t.GoGCCFlags)
	os.Setenv("GOPATH", t.Gopath())
	os.Setenv("PATH", t.Path)
}

// PrintEnvironment - Prints information about the specified environment to the writer.
func PrintEnvironment(w io.Writer, env Environment) error {
	maybe := &maybeWriter{
		delegate: w,
	}
	_, _ = maybe.Write([]byte(fmt.Sprintln("GOROOT:", env.Goroot)))
	_, _ = maybe.Write([]byte(fmt.Sprintln("GOBIN:", env.Gobin)))
	_, _ = maybe.Write([]byte(fmt.Sprintln("GORACE:", env.Gorace)))
	_, _ = maybe.Write([]byte(fmt.Sprintln("GOTOOLDIR:", env.Gotooldir)))
	_, _ = maybe.Write([]byte(fmt.Sprintln("GOGCCFLAGS:", env.GoGCCFlags)))
	_, _ = maybe.Write([]byte(fmt.Sprintln("GOPATH:", env.Gopath())))
	_, _ = maybe.Write([]byte(fmt.Sprintln("PATH:", env.Path)))
	return maybe.err
}

type maybeWriter struct {
	delegate io.Writer
	err      error
}

func (t *maybeWriter) Write(data []byte) (int, error) {
	var (
		written int
	)
	if t.err != nil {
		return 0, t.err
	}

	written, t.err = t.delegate.Write(data)

	return written, t.err
}
