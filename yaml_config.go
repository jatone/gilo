package gilo

import (
	"io"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Example .gopath configuration.
// goroot: "/home/user/.golang/go"
// gobin: "/home/user/.local/programs/bin"
// gotooldir: "gome/user/.golang/tools"
// gorace: "log_path=/tmp/race/report strip_path_prefix=/my/go/sources/"
// gogccflags: "-fPIC -m64 -pthread -fmessage-length=0"
// gopaths:
//   - "/home/user/development/someotherproject"

// YAMLConfig - yaml based configuration.
type YAMLConfig struct {
	io.Reader
}

// Transform - Transforms the specified config based on the underlying
// yaml configuration.
func (t YAMLConfig) Transform(c Config) (Config, error) {
	var raw []byte
	var err error

	if raw, err = ioutil.ReadAll(t.Reader); err != nil {
		return c, err
	}

	// Preset the configuration based on the provided configuration
	loadedConfig := c
	loadedConfig.Gopaths = []string{}

	// Load in values from the yaml configuration
	err = yaml.Unmarshal(raw, &loadedConfig)

	// Merge the gopaths
	loadedConfig.Gopaths = append(c.Gopaths, loadedConfig.Gopaths...)

	return loadedConfig, err
}
