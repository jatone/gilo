package util_test

import (
	. "bitbucket.org/jatone/gilo/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("PathMunger", func() {
	executablePath := "/home/user/.gilo/bin"

	Context("UnixPathMunger", func() {
		Context("Strip", func() {
			It("should strip the specified string from the path", func() {
				munger := UnixPathMunger(JoinPaths(executablePath, "/usr/bin"))
				expected := "/usr/bin"
				Expect(munger.Strip(executablePath)).To(Equal(expected))

				munger = UnixPathMunger(JoinPaths("/usr/bin", executablePath))
				Expect(munger.Strip(executablePath)).To(Equal(expected))

				munger = UnixPathMunger(JoinPaths("/usr/bin", executablePath, "/random/path"))
				expected = JoinPaths("/usr/bin", "/random/path")
				Expect(munger.Strip(executablePath)).To(Equal(expected))
			})

			It("should strip out each matching subpath from the path", func() {
				// from start
				munger := UnixPathMunger(JoinPaths(executablePath, executablePath, "/usr/bin"))
				Expect(munger.Strip(executablePath)).To(Equal("/usr/bin"))

				// from middle
				munger = UnixPathMunger(JoinPaths("/home/doe/.local/programs/bin", executablePath, executablePath, "/usr/bin"))
				Expect(munger.Strip(executablePath)).To(Equal(JoinPaths("/home/doe/.local/programs/bin", "/usr/bin")))

				// from end
				munger = UnixPathMunger(JoinPaths("/home/doe/.local/programs/bin", executablePath, executablePath))
				Expect(munger.Strip(executablePath)).To(Equal("/home/doe/.local/programs/bin"))

				// from start and end
				munger = UnixPathMunger(JoinPaths(executablePath, "/home/doe/.local/programs/bin", executablePath))
				Expect(munger.Strip(executablePath)).To(Equal(JoinPaths("/home/doe/.local/programs/bin")))

				// from non-consecutive middle
				munger = UnixPathMunger(JoinPaths("/home/doe/.rbenv/bin", executablePath, "/home/doe/.local/programs/bin", executablePath, "/usr/bin"))
				Expect(munger.Strip(executablePath)).To(Equal(JoinPaths("/home/doe/.rbenv/bin", "/home/doe/.local/programs/bin", "/usr/bin")))
			})
		})

		Context("Replace", func() {
			It("should replace the specified string from the path", func() {
				executablePath := executablePath
				replacementPath := "/home/user/gopath/bin"

				munger := UnixPathMunger(JoinPaths(executablePath, "/usr/bin"))
				expected := JoinPaths(replacementPath, "/usr/bin")
				Expect(munger.Replace(executablePath, replacementPath, 1)).To(Equal(expected))

				munger = UnixPathMunger(JoinPaths("/usr/bin", executablePath))
				expected = JoinPaths("/usr/bin", replacementPath)
				Expect(munger.Replace(executablePath, replacementPath, 1)).To(Equal(expected))

				munger = UnixPathMunger(JoinPaths("/usr/bin", executablePath, "/random/path"))
				expected = JoinPaths("/usr/bin", replacementPath, "/random/path")
				Expect(munger.Replace(executablePath, replacementPath, 1)).To(Equal(expected))
			})
		})

		Context("Prepend", func() {
			preprendPath := "/home/user/gopath/bin"
			munger := UnixPathMunger("/usr/bin")

			It("should prepend the specified path to the current path", func() {
				expected := JoinPaths(preprendPath, "/usr/bin")
				Expect(munger.Prepend(preprendPath)).To(Equal(expected))
			})

			It("should return the Path field if the prepend path is empty", func() {
				Expect(munger.Prepend("")).To(Equal("/usr/bin"))
			})

			It("should return the prepend path if the Path field is empty", func() {
				Expect(UnixPathMunger("").Prepend(preprendPath)).To(Equal(preprendPath))
			})
		})
	})
})
