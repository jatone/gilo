package util_test

import (
	. "bitbucket.org/jatone/gilo/util"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"errors"
	"os"
	"path/filepath"
)

var _ = Describe("FileResolvers", func() {
	Describe("RecursiveResolver", func() {
		Context("Find", func() {
			pwd, err := os.Getwd()
			if err != nil {
				Fail(err.Error())
			}

			It("should recurse until the specified file is found", func() {
				result, err := RecursiveResolver{Directory: pwd}.Find("src")

				Expect(err).ShouldNot(HaveOccurred())

				expected, err := filepath.Abs(filepath.Join("goenv", "src"))

				Expect(expected).Should(ContainSubstring(result))
			})

			It("should return an error if file cannot be found", func() {
				resolver := RecursiveResolver{Directory: pwd}
				result, err := resolver.Find("I Do Not Exist")
				Expect(err).Should(HaveOccurred())
				Expect(result).To(Equal(""))
			})
		})
	})

	Describe("ErroringResolver", func() {
		Context("Find", func() {
			It("will raise an error", func() {
				result, err := ErroringResolver{Err: errors.New("error")}.Find(".gopath")
				Expect(err).Should(HaveOccurred())
				Expect(result).To(Equal(""))
			})
		})
	})

	Describe("ConstantResolver", func() {
		Context("Find", func() {
			It("should return the Path joined with the specified name", func() {
				result, err := ConstantResolver("/home/user/gopath").Find(".gopath")
				Expect(err).ShouldNot(HaveOccurred())
				Expect(result).To(Equal("/home/user/gopath/.gopath"))
			})
		})
	})
})
