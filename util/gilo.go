package util

import (
	"os"
	"os/user"
	"path/filepath"
)

// Provides utilities methods from the gilo commands.

// environment variable for the directory in which gilo is stored.
const giloBinaryEnvironment string = "GILOBIN"

// MustWorkingDirectory - returns the current working directory.
// or panics.
func MustWorkingDirectory() string {
	var wd string
	var err error
	// Get present working directory
	if wd, err = os.Getwd(); err == nil {
		return wd
	}
	panic(err)
}

// GiloBinary - Returns the directory of the gilo binary.
// Checks the GILOBIN environment variable for a user specified
// value. Otherwise defaults to $HOME/.gilo/bin
var GiloBinary = BinaryResolverFunc(giloBinary)

func giloBinary() (string, error) {
	var usr *user.User
	var err error
	// Determine bin directory
	binDirectory := os.Getenv(giloBinaryEnvironment)
	if len(binDirectory) > 0 {
		return binDirectory, nil
	}

	// User did not specify the bin directory for gilo
	// get current user for default bin directory
	if usr, err = user.Current(); err != nil {
		return "", err
	}

	// Default to $HOME/.gilo/bin
	return filepath.Join(usr.HomeDir, ".gilo", "bin"), nil
}

// BinaryResolver - Used to find the paths to a binary
type BinaryResolver interface {
	Path() (string, error)
}

// BinaryResolverFunc - adapter function allowing pure
// functions to act as a BinaryResolver
type BinaryResolverFunc func() (string, error)

// Path - Returns path computed by the function.
func (t BinaryResolverFunc) Path() (string, error) {
	return t()
}
