package util

import (
	"path/filepath"
	"regexp"
	"strings"
)

var doubleSep = string(filepath.ListSeparator) + string(filepath.ListSeparator)
var singleSep = string(filepath.ListSeparator)

var duplicateSepRegex = regexp.MustCompile(strings.Replace("(^[:]+)|(:[:]+)|([:]+$)", ":", singleSep, -1))

// PathMunger - Utility methods for manipulating
// os paths.
type PathMunger interface {
	Strip(path string) string
	Replace(pattern string, replacement string) string
	Prepend(path string) string
}

// UnixPathMunger - Unix implementation of the path munger.
type UnixPathMunger string

// Strip - Strips the specified subpath from the path.
func (t UnixPathMunger) Strip(subpath string) string {
	// Strip out the specified path.
	striped := strings.Replace(string(t), subpath, "", -1)
	// Replace the :: with a single :
	return UnixPathMunger(striped).clean()
}

// Replace - replaces the supplied pattern with replacement string within the path
// up to n times.
func (t UnixPathMunger) Replace(pattern string, replacement string, n int) string {
	// Replace the pattern with the replacement string
	striped := strings.Replace(string(t), pattern, replacement, n)
	return UnixPathMunger(striped).clean()
}

// cleans the path up of seperators
func (t UnixPathMunger) clean() string {
	return strings.Trim(t.replace(duplicateSepRegex, singleSep), singleSep)
}

func (t UnixPathMunger) replace(pattern *regexp.Regexp, replacement string) string {
	return string(pattern.ReplaceAll([]byte(t), []byte(replacement)))
}

// Prepend - Prepends the specified path to the Path Munger
func (t UnixPathMunger) Prepend(path string) string {
	if len(path) == 0 {
		return string(t)
	}
	if len(string(t)) == 0 {
		return path
	}
	return path + singleSep + string(t)
}

// JoinPaths - Joins the paths together
func JoinPaths(paths ...string) string {
	return strings.Join(paths, singleSep)
}
