package util

import "os"
import "path/filepath"
import "errors"

// Returned when the .gopath file is not found.
var ErrFileNotFound = errors.New("could not detect file")

// FileResolver - Interface for resolving configuration file
type FileResolver interface {
	Find(name string) (string, error)
}

// RecursiveResolver - FileResolver that recurses
// up the directory tree.
type RecursiveResolver struct {
	Directory string
}

// Find - Searches parent directories for the specified
// file until either 1) file is found or 2) root
// directory is reached.
// Will return a could not detect file error if the root directory is reached.
func (t RecursiveResolver) Find(name string) (string, error) {
	path := filepath.Join(t.Directory, name)
	// Get the parent directory.
	parent := filepath.Dir(t.Directory)

	// File exists return path.
	if _, err := os.Stat(path); err == nil {
		return path, nil
	}

	// File does not exist

	// If we are at the root directory
	// return the default value.
	if parent == t.Directory {
		return "", ErrFileNotFound
	}

	// Otherwise recurse into parent directory.
	return RecursiveResolver{Directory: parent}.Find(name)
}

// ErroringResolver - This resolver is for test cases.
type ErroringResolver struct {
	Err error
}

// Find - errors out
func (t ErroringResolver) Find(name string) (string, error) {
	return "", t.Err
}

// ConstantResolver - This resolver is for test cases.
type ConstantResolver string

// Find - Always returns the same base directory
// path for a requested file. (Even if that file doesn't exist)
func (t ConstantResolver) Find(name string) (string, error) {
	return filepath.Join(string(t), name), nil
}
